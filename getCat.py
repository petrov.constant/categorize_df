# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 16:23:28 2018

@author: Konstantin Petrov

Пару функция для поиска признаков
test
"""

import pandas as pd
from collections  import Counter
df=pd.read_excel('exportAllpm.xlsx')
df_good=df[df['Был платный'].notna()]
cr=round(len(df_good)/len(df)*100,2)
category=Counter(df_good['Category'])
category2=Counter(df_good['Unnamed: 8'])
df_good=df_good.reset_index()


# найти все признаки, в которых первое значение - строка
def find_cat(data):
    for name in data.columns:
        s = ''
        s += name
        if (type(data[name][0]) == str):
            s += ' строка,'
        if (data[name].nunique()<=3):
            s += ' мало уникальных'
        if (s!=name):
            print (s)
            
find_cat(df_good)


# конъюнкция двух признаков
def make_conj(data, feature1, feature2):
    data[feature1 + ' + ' + feature2] = data[feature1].astype(str) + ' + ' + data[feature2].astype(str)
    return (data)

# пример использования
conj=make_conj(df_good, 'Category', 'Unnamed: 8')

col_two=Counter(conj['Category + Unnamed: 8'])


from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
le.fit(df_good['Category + Unnamed: 8'])
df_good['le'] = le.transform(df_good['Category + Unnamed: 8'])
